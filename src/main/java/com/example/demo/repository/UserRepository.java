package com.example.demo.repository;

import com.example.demo.entity.User;
import org.hibernate.annotations.LazyToOne;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
